import {
  GET_KYC_LIST,
  SET_LOADING,
  SET_DISMISS_LOADING,
} from "../action-types";

const initialState = {
  kycList: [],
  loading: false,
};

export const reducerActions = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_KYC_LIST:
      return { ...state, kycList: payload };
    case SET_LOADING:
      return { ...state, loading: true };
    case SET_DISMISS_LOADING:
      return { ...state, loading: false };
    default:
      return state;
  }
};

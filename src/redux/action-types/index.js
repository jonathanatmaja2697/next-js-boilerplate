const GET_KYC_LIST = "get-kyc-list";
const SET_LOADING = "set-loading";
const SET_DISMISS_LOADING = "set-dismiss-loading";

export { GET_KYC_LIST, SET_LOADING, SET_DISMISS_LOADING };

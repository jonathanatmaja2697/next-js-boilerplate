import { useEffect, useLayoutEffect, useState } from "react";

const wp = (val = 0) => {
  const [width, height] = useWindowSize();
  return (width * val) / 100;
};

const hp = (val = 0) => {
  const [width, height] = useWindowSize();
  return (height * val) / 100;
};

const useWindowSize = () => {
  const [size, setSize] = useState([0, 0]);
  useEffect(() => {
    const updateSize = () => {
      setSize([window.innerWidth, window.innerHeight]);
    };

    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);
  return size;
};

export { wp, hp };

import React from "react";
import { Footer } from "../../components";
import Navbar from "../../components/Navbar";

const Detail = () => {
  return (
    <div>
      <Navbar active={2} />
      <Footer />
    </div>
  );
};

export default Detail;

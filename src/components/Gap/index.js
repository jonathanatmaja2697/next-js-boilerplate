import React from "react";

const Gap = ({height = 0, width = 0}) => {
  return <div style={styles.gap(height, width)}></div>;
};

export default Gap;

const styles = {
  gap: (height, width) => ({
    height: height + "px",
    width: width + "px",
    opacity: 0,
  }),
};

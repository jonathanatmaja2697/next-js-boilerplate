import Header from "./Header";
import Footer from "./Footer";
import Content from "./Content";
import Container from "./Container";
import Gap from "./Gap";
import Loading from "./Loading";

export { Header, Footer, Content, Container, Gap, Loading };

import { AppBar, Container, Toolbar } from "@mui/material";
import React, { useState } from "react";
import { hp } from "../../functions";

const Header = () => {
  return (
    <AppBar position="static" color="transparent" sx={styles.container(hp(10))}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <img src="/assets/logo.png" style={styles.icon(hp(16), hp(5))} />
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;

const styles = {
  icon: (width, height) => ({
    width,
    height,
  }),
  container: (height) => ({ height, justifyContent: "center" }),
};
